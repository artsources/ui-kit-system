/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 12.04.2015
 * Time: 17:24
 */
angular.module('system.main', [
    'system.directives',
    'system.filters',
    'system.services'
]);