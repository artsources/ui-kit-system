/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:27
 */
(function (angular) {
    'use strict';

    angular.module('sys.directive.height-resizer', [])
        .directive('sysHeightResizer', ['sysArrayUtils', function (ArrayUtils) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var params = scope.$eval(attrs.sysHeightResizer) || {};

                    // Find element to trace changes of height among nearest elements
                    var tracedElement = element.closest(params.trace);
                    // If there is no such element then directive does nothing
                    if (tracedElement.length === 0) {
                        return;
                    }

                    // Find nearest parent with class
                    var container = params.container ? element.closest(params.container) : element.parent();
                    params.top = ArrayUtils.wrap(params.top);
                    params.bottom = ArrayUtils.wrap(params.bottom);

                    var ElementType = {
                        TOP: 'top',
                        BOTTOM: 'bottom'
                    };

                    var helper = {
                        elements: {
                            top: [],
                            bottom: []
                        },
                        getTopPos: function() {
                            return params[ElementType.TOP].length > 0 ? this.getPos(ElementType.TOP) : element.offset().top - container.offset().top;
                        },
                        getBottomPos: function() {
                            return params[ElementType.BOTTOM].length > 0 ? this.getPos(ElementType.BOTTOM) : container.height();
                        },
                        getPos: function (elementType) {
                            for (var selectorIdx = 0; selectorIdx < params[elementType].length; selectorIdx++) {
                                var elSelector = params[elementType][selectorIdx];
                                if (!this.elements[elementType][selectorIdx]) {
                                    this.elements[elementType][selectorIdx] = container.find(elSelector).first();
                                    this.elements[elementType][selectorIdx] = this.elements[elementType][selectorIdx].length > 0 ? this.elements[elementType][selectorIdx] : null;
                                }
                            }
                            var pos = 0;
                            for (var elIdx = 0; elIdx < this.elements[elementType].length; elIdx++) {
                                var el = this.elements[elementType][elIdx];
                                if (el) {
                                    pos += el.offset().top - container.offset().top;
                                } else {
                                    pos = -1;
                                    break;
                                }
                            }
                            return pos;
                        },
                        updateHeight: function(top, bottom) {
                            if (bottom > 0 && top > 0) {
                                element.height(bottom - top);
                            }
                        },
                        resizeHandler: function () {
                            scope.$apply();
                        }
                    };

                    element.css({'max-height': 'none', 'min-height': '0px', height: 'auto'});

                    // Calculate height first time
                    helper.updateHeight(helper.getTopPos(), helper.getBottomPos());
                    addResizeListener(tracedElement[0], helper.resizeHandler);

                    scope.$on('$destroy', function() {
                        removeResizeListener(tracedElement[0], helper.resizeHandler);
                    });

                    scope.$watch(function () {
                        var top = helper.getTopPos();
                        var bottom = helper.getBottomPos();
                        return { height: tracedElement.height(), top: top, bottom: bottom, flags: { top: top >= 0, bottom: bottom >= 0, height: element.height() != (bottom - top) } };
                    }, function (params) {
                        helper.updateHeight(params.top, params.bottom);
                    }, true);
                }
            };
        }]);
}) (angular);