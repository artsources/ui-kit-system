/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:27
 */
(function (angular) {
    'use strict';

    angular.module('sys.directive.show', [])
        .directive('sysShow', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                link: function ($scope, element, attr) {
                    $scope.$watch(function () {
                        return $parse(attr.sysShow)($scope);
                    }, function (show) {
                        element.toggle(show);
                    });
                }
            };
        }]);
}) (angular);