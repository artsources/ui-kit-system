/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:27
 */
(function (angular) {
    'use strict';

    angular.module('sys.directive.stop-propagation', [])
        .directive('sysStopPropagation', [function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    element.on('click.sysStopPropagation' + scope.$id, function (event) {
                        event.stopPropagation();
                    });

                    scope.$on('$destroy', function() {
                        element.off('click.sysStopPropagation' + scope.$id);
                    });
                }
            };
        }]);
}) (angular);