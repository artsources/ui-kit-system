/**
 * Copyright (c)
 * Date: 18.04.14 8:58
 * File: ng-jquery-directives.js
 *
 * @author akalashnikov
 */

(function (angular) {
    "use strict";

    angular.module('sys.directive.anchor-scroll-offset', [])
        .directive('sysAnchorScrollOffset', [
            '$anchorScroll',
            function ($anchorScroll) {
                return {
                    restrict: 'A', // Restrict it to be an attribute in this case
                    scope: {},
                    link: function ($scope, element) {
                        // Set offset by height of the element to which directive is attached
                        $anchorScroll.yOffset = element;
                    }
                };
            }
        ]);
})(angular);
