/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:27
 */
(function (angular) {
    'use strict';

    angular.module('sys.directive.height-trace', [])
        .directive('sysHeightTrace', [function () {
            return {
                restrict: 'A',
                link: function ($scope, element, attrs) {
                    var tracedElement = angular.element(attrs.sysHeightTrace);

                    var helper = {
                        updateHeight: function() {
                            element.height(tracedElement.height());
                        },
                        resizeHandler: function () {
                            $scope.$apply();
                        }
                    };

                    // Calculate height first time
                    helper.updateHeight();
                    addResizeListener(tracedElement[0], helper.resizeHandler);

                    $scope.$on('$destroy', function() {
                        removeResizeListener($scope.tracedElement[0], helper.resizeHandler);
                    });

                    $scope.$watch(function () {
                        return { height: tracedElement.height() };
                    }, function () {
                        helper.updateHeight();
                    }, true);
                }
            };
        }]);
}) (angular);