(function (angular) {
    'use strict';

    /**
     * types:
     *      ADDRESS
     *      NAME
     *      PARTY
     *      EMAIL
     *      BANK
     *
     * bounds: (http://dadata.userecho.com/topic/390757)
     *      region-area
     *      city-settlement
     *      street
     *      house
     *
     * You need to link styles and script of da-data:
     *
     *      <!-- dadata libs -->
     *      <link href="https://dadata.ru/static/css/lib/suggestions-15.12.css" type="text/css" rel="stylesheet" />
     *      <script src="https://dadata.ru/static/js/lib/jquery.suggestions-15.12.min.js"></script>
     */
    angular.module('sys.directive.da-data', [])
        .provider('sysDaDataConfig', [function () {
            var baseOptions = {
                token: ''
            };
            return {
                config: function (params) {
                    angular.extend(baseOptions, params);
                },
                $get: [function () {
                    return baseOptions;
                }]
            };
        }])
        .directive('sysDaData', ['sysDaDataConfig', function (daDataConfig) {
            //noinspection JSUnusedGlobalSymbols
            return {
                restrict: 'A',
                scope: {
                    type: '@daDataType',
                    bounds: '@daDataBounds',
                    data: '=sysDaData',
                    value: '=ngModel'
                },
                link: function ($scope, element) {
                    //noinspection JSUnusedGlobalSymbols
                    element.suggestions({
                        serviceUrl: 'https://dadata.ru/api/v2',
                        token: daDataConfig.token,
                        type: $scope.type.toUpperCase(),
                        bounds: $scope.bounds || undefined,
                        onSelect: function (suggestion) {
                            $scope.data = suggestion.data;
                            $scope.value = element.val().trim();
                            $scope.$apply();
                        }
                    });
                }
            };
        }]);
})(angular);
