/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:27
 */
(function (angular) {
    'use strict';

    angular.module('sys.directive.single-click', [])
        .directive('sysSingleClick', ['$parse', '$timeout', function ($parse, $timeout) {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    var DELAY = 300;

                    var handler = $parse(attr.sysSingleClick);
                    var clicksCount = 0;
                    var timer = null;

                    element.on('click.sysSingleClick' + scope.$id, function (event) {
                        // Count clicks
                        clicksCount++;
                        if (clicksCount === 1) {
                            timer = $timeout(function () {
                                handler(scope, {$event: event});
                                // After action performed, reset counter
                                clicksCount = 0;
                            }, DELAY);
                        } else {
                            // Prevent single-click action
                            $timeout.cancel(timer);
                            // After action performed, reset counter
                            clicksCount = 0;
                        }
                    });

                    scope.$on('$destroy', function() {
                        element.off('click.sysSingleClick' + scope.$id);
                    });
                }
            };
        }]);
}) (angular);