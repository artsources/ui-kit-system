/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:26
 */
(function (angular) {
    'use strict';

    angular.module('system.directives', [
        'sys.directive.anchor-scroll-offset',
        'sys.directive.da-data',
        'sys.directive.height-resizer',
        'sys.directive.height-trace',
        'sys.directive.show',
        'sys.directive.single-click',
        'sys.directive.stop-propagation'
    ]);

}) (angular);