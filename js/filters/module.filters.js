/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 01.05.2015
 * Time: 11:16
 */
(function (angular) {
    'use strict';

    angular.module('system.filters', [])
        .filter('empty', ['$filter', function ($filter) {
            return function (list, filter) {
                if (angular.isUndefined(filter)) {
                    return list.length === 0;
                }
                return $filter('filter')(list, filter).length === 0;
            };
        }])
        .filter('notEmpty', ['$filter', function ($filter) {
            return function (list, filter) {
                if (angular.isUndefined(filter)) {
                    return list.length > 0;
                }
                return $filter('filter')(list, filter).length > 0;
            };
        }])
        .filter('img', [function () {
            return function (src) {
                return src || '//:0';
            };
        }])
        .filter('limitFrom', [function () {
            return function (list, from) {
                return list.slice(from);
            };
        }])
        .filter('needToTruncate', [function () {
            return function (str, charCount) {
                if (!str) {
                    return false;
                }
                // Strip tags using browser
                var tmp = angular.element('<div>');
                tmp.html(str);
                str = tmp.text();
                return charCount < str.length;
            };
        }])
        .filter('noNeedToTruncate', ['$filter', function ($filter) {
            return function (str, charCount) {
                return !$filter('needToTruncate')(str, charCount);
            };
        }])
        .filter('truncate', [function () {
            return function (str, charCount) {
                if (!str) {
                    return '';
                }
                // Strip tags using browser
                var tmp = angular.element('<div>');
                tmp.html(str);
                str = tmp.text();
                // Truncate plain text of the string
                var result = str;
                if (charCount < str.length) {
                    result = str.substr(0, charCount - 3) + '...';
                }
                return result;
            };
        }])
        .filter('formatDateTime', ['$filter', function($filter) {
            return function(input) {
                var date = input || '';
                if (!date) {
                    return 'Не задано';
                }
                return $filter('date')(date, 'dd.MM.yyyy HH:mm');
            };
        }])
        .filter('formatDate', ['$filter', function($filter) {
            return function(input) {
                var date = input || '';
                if (!date) {
                    return 'Не задано';
                }
                return $filter('date')(date, 'dd.MM.yyyy');
            };
        }])
        .filter('formatTime', ['$filter', function($filter) {
            return function(input) {
                var date = input || '';
                if (!date) {
                    return 'Не задано';
                }
                return $filter('date')(date, 'HH:mm');
            };
        }])
        .filter('serverDateTime', ['$filter', function($filter) {
            return function (date) {
                var timezone = $filter('date')(date, 'Z'); // Format: '+HHmm'
                // Add separator ':' between hours and minutes: '+HH:mm'
                var colonPosition = 3;
                timezone = [timezone.slice(0, colonPosition), ':', timezone.slice(colonPosition)].join('');
                return $filter('date')(date, 'yyyy-MM-ddTHH:mm:ss.sss') + timezone;
            };
        }])
        .filter('serverDateNoSC', ['$filter', function($filter) {
            return function (date) {
                return $filter('date')(date, 'yyyy-MM-ddTHH:mm:ss.sssZ');
            };
        }]);

}) (angular);
