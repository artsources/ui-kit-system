angular.module('sys.service.logging', [])
    .constant('sysLoggingLevelType', {
        NONE: 0,
        ERROR: 1,
        WARNING: 2,
        INFO: 3,
        DEBUG: 4
    })
    .provider('sysLoggingService', ['sysLoggingLevelType', function(LoggingLevelType) {
        var baseOptions = {
            prefix: '',
            logLevel: LoggingLevelType.INFO
        };
        return {
            config: function (params) {
                angular.extend(baseOptions, params);
            },
            $get: [function() {
                function Logger(params) {
                    this.options = {
                        prefix: ''
                    };
                    angular.extend(this.options, params);
                }
                Logger.prototype = {
                    _invokeFunction: function(functionName, logLevel, args) {
                        if (logLevel <= baseOptions.logLevel) {
                            var basePrefix = baseOptions.prefix ? '[' + baseOptions.prefix + ']' : '';
                            var prefix = this.options.prefix ? '[' + this.options.prefix + ']' : '';
                            if (typeof args[0] === 'string') {
                                args[0] = basePrefix + prefix + ' ' + args[0];
                            } else {
                                Array.prototype.splice.call(args, 0, 0, basePrefix + prefix);
                            }
                            console[functionName].apply(console, args);
                        }
                    },
                    log: function() {
                        this._invokeFunction('log', LoggingLevelType.INFO, arguments);
                    },
                    debug: function() {
                        this._invokeFunction('debug', LoggingLevelType.DEBUG, arguments);
                    },
                    info: function() {
                        this._invokeFunction('info', LoggingLevelType.INFO, arguments);
                    },
                    warn: function() {
                        this._invokeFunction('warn', LoggingLevelType.WARNING, arguments);
                    },
                    error: function() {
                        this._invokeFunction('error', LoggingLevelType.ERROR, arguments);
                    }
                };
                return Logger;
            }]
        };
    }]);