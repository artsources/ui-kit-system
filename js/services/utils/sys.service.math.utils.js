/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 26.04.2015
 * Time: 15:33
 */
(function (angular) {
    'use strict';

    angular.module('sys.service.math.utils', [])
        .factory('sysMathUtils', [function () {
            /**
             * Decimal adjustment of a number.
             *
             * @param {string}  type  The type of adjustment.
             * @param {number}  value The number.
             * @param {int}     exp   The exponent (the 10 logarithm of the adjustment base).
             * @returns {number} The adjusted value.
             */
            function decimalAdjust(type, value, exp) {
                // If the exp is undefined or zero...
                if (typeof exp === 'undefined' || +exp === 0) {
                    return Math[type](value);
                }
                value = +value;
                exp = +exp;
                // If the value is not a number or the exp is not an integer...
                if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                    return NaN;
                }
                // Shift
                value = value.toString().split('e');
                value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                // Shift back
                value = value.toString().split('e');
                return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
            }

            return {
                round10: function (value, exp) {
                    return decimalAdjust('round', value, exp);
                },
                floor10: function (value, exp) {
                    return decimalAdjust('floor', value, exp);
                },
                ceil10: function (value, exp) {
                    return decimalAdjust('ceil', value, exp);
                }
            };
        }]);
})(angular);