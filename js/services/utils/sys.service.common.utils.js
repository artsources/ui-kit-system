/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 25.11.2014
 * Time: 18:34
 */

(function (angular) {
    'use strict';
    angular.module('sys.service.common.utils', [])
        .factory('sysCommonUtils', [
            '$q', '$timeout', 'sysAlertsService', 'sysLoggingService',
            function ($q, $timeout, alertsService, Logger) {
                var logger = new Logger({prefix: 'common utils'});
                return {
                    /**
                     * @param {*} [value] Value used to reject deferred.
                     * @return {promise} Returns rejected promise.
                     */
                    getRejectedPromise: function (value) {
                        var deferred = $q.defer();
                        deferred.reject(value);
                        return deferred.promise;
                    },
                    /**
                     * @param {*} [value] Value used to resolve deferred.
                     * @return {promise} Returns resolved promise.
                     */
                    getResolvedPromise: function (value) {
                        var deferred = $q.defer();
                        deferred.resolve(value);
                        return deferred.promise;
                    },
                    /**
                     * Method handles error during interaction with the backend.
                     * It rejects deferred with specified error message.
                     *
                     * @param {string} message Error message to display user.
                     * @param {{[reject]: function, [error]: string, [location]: string}} [params] (optional) Function to reject deferred and error description.
                     * @returns {*}
                     */
                    errorHandler: function (message, params) {
                        params = params || {};
                        alertsService.showErrors(message);
                        if (params.error) {
                            var errorDetails = (params.location ? '[' + params.location + '] ' : '') + params.error;
                            logger.error(errorDetails);
                        }
                        return angular.isUndefined(params.reject) ? $q.reject(message) : params.reject(message);
                    },
                    /**
                     * Method handles unexpected error during interaction with the backend.
                     *
                     * @param {{[reject]: function, [error]: string, [location]: string}} [params] (optional) Function to reject deferred and error description.
                     * @returns {*}
                     */
                    unexpectedErrorHandler: function (params) {
                        return this.errorHandler('Возникли непредвиденные проблемы. Попробуйте ещё раз или обратитесь в техническую поддержку.', params);
                    },
                    /**
                     * Method checks whether device is touch.
                     *
                     * @returns {boolean} Returns true if device is touch device otherwise false.
                     */
                    isTouchDevice: function () {
                        return ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
                    },
                    /**
                     * Method runs digest safely.
                     * It uses $timeout to invoke digest.
                     */
                    runDigest: function () {
                        $timeout(function () {
                        }, 0);
                    },
                    /**
                     * Method gets data field from result of success promise.
                     *
                     * @return {promise} Returns http promise.
                     */
                    unwrapRequestResult: function (httpPromise) {
                        return httpPromise.then(function (result) {
                            return result.data;
                        });
                    }
                };
            }
        ]);
}) (angular);