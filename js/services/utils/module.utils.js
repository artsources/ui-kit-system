/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 26.04.2015
 * Time: 14:49
 */
(function (angular) {
    'use strict';

    angular.module('system.services.utils', [
        'sys.service.array.utils',
        'sys.service.math.utils',
        'sys.service.common.utils',
        'sys.service.date.utils'
    ]);
}) (angular);