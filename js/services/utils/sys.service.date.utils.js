/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 26.04.2015
 * Time: 15:33
 */
(function (angular) {
    'use strict';

    angular.module('sys.service.date.utils', [])
        .factory('sysDateUtils', [function () {
            return {
                /**
                 * Method clear time of date.
                 *
                 * @param {Date} date Specified date.
                 * @returns {Date} Returns new date contains only year, month, day info.
                 */
                clearTime: function(date) {
                    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
                },
                /**
                 * Method determines whether there is date in specified range.
                 *
                 * @param {Date} date Date to check.
                 * @param {Date} rangeStart Start of date range.
                 * @param {Date} rangeEnd End of date range.
                 * @returns {boolean} Returns true if specified date range contains date otherwise false.
                 */
                dateInRange: function(date, rangeStart, rangeEnd){
                    var start = this.clearTime(rangeStart);
                    var end = this.clearTime(rangeEnd);
                    var check = this.clearTime(date);

                    return start <= check && check <= end;
                },
                /**
                 * Method checks overlap of date ranges.
                 *
                 * @param {Date} rangeAStart Start of first date range.
                 * @param {Date} rangeAEnd End of first date range.
                 * @param {Date} rangeBStart Start of second date range.
                 * @param {Date} rangeBEnd End of second date range.
                 * @returns {boolean} Returns true if date ranges overlap otherwise false.
                 */
                isRangesOverlap: function(rangeAStart, rangeAEnd, rangeBStart, rangeBEnd) {
                    // Inverse condition of no overlap
                    return !(rangeAEnd < rangeBStart || rangeBEnd < rangeAStart);
                },
                /**
                 * Method checks equality of dates.
                 *
                 * @param {Date} date1
                 * @param {Date} date2
                 * @returns {boolean} Returns true if dates equals, otherwise false.
                 */
                equals: function(date1, date2) {
                    return date1 !== null && date2 !== null ? date1.getTime() == date2.getTime() : date1 === null && date2 === null;
                },
                /**
                 * @param {Date} [date] (optional) Date to calculate minimal date. If not specified use current date.
                 * @returns {Date} Returns minimal day date.
                 */
                getMinDayDate: function(date) {
                    date = date || new Date();
                    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
                },
                /**
                 * @param {Date} [date] (optional) Date to calculate maximal date. If not specified use current date.
                 * @returns {Date} Returns maximal day date.
                 */
                getMaxDayDate: function(date) {
                    date = date || new Date();
                    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999);
                },
                /**
                 * Method converts ISO8601 date string to date.
                 *
                 * @param {string} dateStr Date string.
                 * @returns {Date|string} Returns date given from string if string satisfies ISO8601 pattern, otherwise origin string.
                 */
                strToDate: function (dateStr) {
                    function toInt(str) {
                        return parseInt(str, 10);
                    }

                    var R_ISO8601_STR = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;
                    //                    1        2       3         4          5          6          7          8  9     10      11
                    var match = dateStr.match(R_ISO8601_STR);
                    if (match) {
                        var date = new Date(0),
                            tzHour = 0,
                            tzMin = 0,
                            dateSetter = match[8] ? date.setUTCFullYear : date.setFullYear,
                            timeSetter = match[8] ? date.setUTCHours : date.setHours;

                        if (match[9]) {
                            tzHour = toInt(match[9] + match[10]);
                            tzMin = toInt(match[9] + match[11]);
                        }
                        dateSetter.call(date, toInt(match[1]), toInt(match[2]) - 1, toInt(match[3]));
                        var h = toInt(match[4] || 0) - tzHour;
                        var m = toInt(match[5] || 0) - tzMin;
                        var s = toInt(match[6] || 0);
                        var ms = Math.round(parseFloat('0.' + (match[7] || 0)) * 1000);
                        timeSetter.call(date, h, m, s, ms);
                        return date;
                    }
                    return dateStr;
                }
            };
        }]);
})(angular);