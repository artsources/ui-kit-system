/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 26.04.2015
 * Time: 14:49
 */
(function (angular) {
    'use strict';

    angular.module('sys.service.array.utils', [])
        .factory('sysArrayUtils', [function () {
            return {
                /**
                 * Method prepares array of objects.
                 * If constructor of object is specified then all origin objects are wrapped by specified class.
                 * This method doesn't change original array.
                 *
                 * @param {object|Array|null} list Array of objects, single object or null if there aren't objects.
                 * @param {function} [constructor] (optional) Constructor of object.
                 * @returns {Array} Returns array of objects or empty array if origin list is empty or null.
                 */
                wrap: function (list, constructor) {
                    var wrappedList = [];
                    if (typeof list === 'undefined' || list === null || list === '') {
                        wrappedList = [];
                    } else if (list instanceof Array) {
                        wrappedList = wrappedList.concat(list);
                    } else {
                        wrappedList.push(list);
                    }
                    if (typeof constructor === 'function') {
                        for (var i = 0; i < wrappedList.length; i++) {
                            /*jshint newcap:false */
                            wrappedList[i] = new constructor(wrappedList[i]);
                            /*jshint newcap:true */
                        }
                    }
                    return wrappedList;
                },
                /**
                 * Method removes element specified by index from array.
                 * This method changes original array.
                 *
                 * @param {Array} array Array to remove element
                 * @param {int|object} item Index of element or element to remove from array.
                 * @returns {Array} Returns removed items or empty array if there is no removed item.
                 */
                remove: function (array, item) {
                    if (array === undefined || array === null) {
                        throw new TypeError('array is null or not defined');
                    }
                    var index;
                    if (typeof item === 'number') {
                        index = item;
                    } else {
                        index = array.indexOf(item);
                    }
                    return index >= 0 ? array.splice(index, 1) : [];
                },
                /**
                 * Method finds and returns last element of array.
                 *
                 * @param {Array} array Array to find last element.
                 * @returns {object|null} Returns whether last element of array if exists or null if array is empty.
                 */
                last: function (array) {
                    return array && array.length > 0 ? array[array.length - 1] : null;
                },
                /**
                 * Method checks there is element in array.
                 *
                 * @param {Array} array Array to check element exists.
                 * @param {object} element Element to test.
                 * @returns {boolean} Returns true if there is element in array otherwise false.
                 */
                contains: function (array, element) {
                    return array.indexOf(element) >= 0;
                },
                /**
                 * @param {Array} array Array to check.
                 * @returns {boolean} Returns true if array is empty, otherwise false.
                 */
                empty: function(array) {
                    return array.length === 0;
                }
            };
        }]);
}) (angular);