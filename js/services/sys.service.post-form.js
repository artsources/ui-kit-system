/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 21.12.2014
 * Time: 17:14
 */
angular.module('sys.service.post-form', [])
    .provider('sysPostFormService', [function () {
        return {
            $get: ['$http', function ($http) {
                return function (url, data) {
                    return $http({
                        method: 'POST',
                        url: url,
                        data: $.param(data),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function (result) {
                        return result.data;
                    });
                };
            }]
        };
    }]);