angular.module('sys.service.alerts', [])
    .constant('sysAlertType', {
        INFO: 'info',
        WARNING: 'warning',
        ERROR: 'error'
    })
    .factory('sysAlert', ['sysAlertType', function(AlertType) {
        var ALERT_APPEAL = {};
        ALERT_APPEAL[AlertType.INFO] = 'Информация!';
        ALERT_APPEAL[AlertType.WARNING] = 'Внимание!';
        ALERT_APPEAL[AlertType.ERROR] = 'Ошибка!';

        function Alert(data) {
            var alertData = data || {};
            if (typeof alertData === 'string') {
                alertData = { message: alertData };
            }

            this.type = alertData.type || AlertType.INFO;
            this.appeal = alertData.appeal || ALERT_APPEAL[this.type];
            this.message = alertData.message || '';
            this.details = alertData.details || '';
        }

        Alert.getAppealByType = function (alertType) {
            return ALERT_APPEAL[alertType];
        };

        return (Alert);
    }])
    .factory('sysAlertsService', ['sysHandlerService', 'sysAlert', 'sysAlertType', 'sysArrayUtils', function(handlerService, Alert, AlertType, ArrayUtils) {
        var SERVICE_NAME = 'sysAlertsService';
        var ServiceEventType = {
            SHOW_ALERTS: SERVICE_NAME + '-show-alert'
        };

        var helper = {
            showSpecifiedAlerts: function (alerts, alertType) {
                var wrappedAlerts = ArrayUtils.wrap(alerts, Alert);
                angular.forEach(wrappedAlerts, function (alert) {
                    alert.type = alertType;
                    alert.appeal = Alert.getAppealByType(alertType);
                });
                handlerService.notify(ServiceEventType.SHOW_ALERTS, {alerts: wrappedAlerts});
            }
        };
        return {
            showErrors: function (alerts) {
                helper.showSpecifiedAlerts(alerts, AlertType.ERROR);
            },
            showWarnings: function (alerts) {
                helper.showSpecifiedAlerts(alerts, AlertType.WARNING);
            },
            showInfo: function (alerts) {
                helper.showSpecifiedAlerts(alerts, AlertType.INFO);
            },
            onShowAlerts: function ($scope, handler) {
                handlerService.setHandler(ServiceEventType.SHOW_ALERTS, $scope, handler);
            }
        };
    }]);
