/**
 * Created with IntelliJ IDEA (in accordance with File | Settings | File and Code Templates).
 * User: tohasan
 * Date: 12.04.2015
 * Time: 17:33
 */
angular.module('system.services', [
    'system.services.utils',

    'sys.service.alerts',
    'sys.service.handler',
    'sys.service.logging',
    'sys.service.post-form'
]);