angular.module('sys.service.handler', [])
    .factory('sysHandlerService', ['$rootScope', function($rootScope) {
        return {
            notify: function(eventType, params) {
                $rootScope.$broadcast(eventType, params);
            },
            setHandler: function(eventType, $scope, handler) {
                $scope.$on(eventType, function (event, message) {
                    handler(message);
                });
            }
        };
    }]);
