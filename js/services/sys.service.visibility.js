/**
 * author: akalashnikov
 * date: 18.01.2016 10:52
 */

(function (angular) {
    'use strict';

    angular.module('sys.service.visibility', ['sys.service.handler'])
        .factory('sysVisibilityService', [
            '$document', 'sysHandlerService',
            function ($document, handlerService) {
                var SERVICE_NAME = 'sysVisibilityService';
                var ServiceEventType = {
                    SHOW_PAGE: SERVICE_NAME + '-show-page',
                    HIDE_PAGE: SERVICE_NAME + '-hide-page'
                };

                // Set the name of the hidden property and the change event for visibility
                // MDN: https://developer.mozilla.org/en-US/docs/Web/Guide/User_experience/Using_the_Page_Visibility_API
                var hidden;
                var visibilityChangeEvent;
                var visibilityState;
                var doc = document;

                if (typeof doc.hidden !== "undefined") {
                    // Opera 12.10 and Firefox 18 and later support
                    hidden = "hidden";
                    visibilityState = "visibilityState";
                    visibilityChangeEvent = "visibilitychange";
                } else if (typeof doc.mozHidden !== "undefined") {
                    hidden = "mozHidden";
                    visibilityState = "mozVisibilityState";
                    visibilityChangeEvent = "mozvisibilitychange";
                } else if (typeof doc.msHidden !== "undefined") {
                    hidden = "msHidden";
                    visibilityState = 'msVisibilityState';
                    visibilityChangeEvent = "msvisibilitychange";
                } else if (typeof doc.webkitHidden !== "undefined") {
                    hidden = "webkitHidden";
                    visibilityState = 'webkitVisibilityState';
                    visibilityChangeEvent = "webkitvisibilitychange";
                }

                /**
                 * Visibility states of page visibility API
                 *
                 * @type {{VISIBLE: string, HIDDEN: string, PRERENDER: string, UNLOADED: string}}
                 */
                var VisibilityStateType = {
                    // visible : the page content may be at least partially visible. In practice this means that the page is the foreground tab of a non-minimized window.
                    VISIBLE: 'visible',
                    // hidden : the page content is not visible to the user. In practice this means that the document is either a background tab or part of a minimized window, or the OS screen lock is active
                    HIDDEN: 'hidden',
                    // prerender : the page content is being prerendered and is not visible to the user (considered hidden for purposes of document.hidden). The document may start in this state, but will never transition to it from another value. Note: browser support is optional.
                    PRERENDER: 'prerender',
                    // unloaded : the page is being unloaded from memory. Note: browser support is optional.
                    UNLOADED: 'unloaded'
                };

                var helper = {
                    /**
                     * Method checks browser support of the Page Visibility API.
                     *
                     * @returns {boolean} Returns true if browser supports page visibility, otherwise false.
                     */
                    support: function () {
                        return typeof doc[hidden] !== "undefined";
                    },
                    /**
                     * @returns {undefined|boolean} Returns true if page is hidden otherwise false.
                     * If browser does not support API then returns undefined.
                     */
                    hidden: function () {
                        return doc[hidden];
                    },
                    /**
                     * @returns {undefined|boolean} Returns true if page is visible otherwise false.
                     * If browser does not support API then returns undefined.
                     */
                    visible: function () {
                        return !doc[hidden];
                    },
                    /**
                     * @returns {undefined|string} Returns visibility state of the page.
                     */
                    visibilityState: function () {
                        return doc[visibilityState];
                    }
                };

                if (helper.support()) {
                    // Set event handler only if page visibility API is supported.
                    $document.on(visibilityChangeEvent, function () {
                        // If needed we can check document.visibilityState

                        if (helper.hidden()) {
                            handlerService.notify(ServiceEventType.HIDE_PAGE);
                        } else {
                            handlerService.notify(ServiceEventType.SHOW_PAGE);
                        }
                    });
                }

                return {
                    support: function () {
                        return helper.support();
                    },
                    hidden: function () {
                        return helper.hidden();
                    },
                    visible: function () {
                        return helper.visible();
                    },
                    onHidePage: function ($scope, handler) {
                        handlerService.setHandler(ServiceEventType.HIDE_PAGE, $scope, handler);
                    },
                    onShowPage: function ($scope, handler) {
                        handlerService.setHandler(ServiceEventType.SHOW_PAGE, $scope, handler);
                    }
                };
            }
        ]);
})(angular);